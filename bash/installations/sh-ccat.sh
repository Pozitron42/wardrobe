#!/bin/bash

ccatVer="1.1.0"
wget "https://github.com/jingweno/ccat/releases/download/v$ccatVer/linux-amd64-$ccatVer.tar.gz"
tar -zxvf linux-amd64-$ccatVer.tar.gz
cd linux-amd64-$ccatVer
cp ccat /bin/