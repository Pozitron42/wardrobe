#!/bin/bash

docker pull portainer/portainer


docker volume create portainer_data
docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer

docker ps

$custom_pwd = "toor"

docker run --rm httpd:2.4-alpine htpasswd -nbB admin "$custom_pwd" | cut -d ":" -f 2

echo
echo
echo "You can access with http://localhost:9000"
echo "User: admin"
echo "Password: $custom_pwd"