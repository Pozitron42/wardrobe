#!/bin/bash

# retreive raw list
scan=$(debsecan --suite jessie --only-fixed | grep -i "remotely.*high" | cut -d ' ' -f2 | uniq | tr '\n' ' ')

# package description on filtered list
desc=$(dpkg -l $scan)

echo "$desc"