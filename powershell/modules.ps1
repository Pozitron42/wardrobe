$modulesList = @(
    "PowerHTML",
    "QRCodes",
    "PScx",
    "Dimmo",
    "ShowUI",
    "ImportExcel",
    "WakeOnLan",
    "SecurityFever"
)

$qText = "Installing module {module}? [Y/n]"

$modulesList | % {
    
    $module = $_
    
    Write-Host "$((Find-Module $module).Description)"
    
    if(!(Get-Module $module -ListAvailable)) {
        if((Read-Host ($qText -replace "\{module\}", "$module")) -in @("y", "Y", "yes", "")) {
            
            Write-Host "--> Installing module $module" -fore "Yellow"
            
            Install-Module $module -Force -Verbose
            
        }
        
    }
    
}